package hwo2014

trait TrackPiece {
  val index: Int
  val length: Int => Double
  val isCurve: Boolean
  val switch: Boolean
  val lanes: List[Lane]
}
case class Curve(index: Int, angle: Double, pieceRadius: Double, lanes: List[Lane], switch: Boolean = false) extends TrackPiece {
  val length: Int => Double = lane => 
    2 * Math.PI * radius(lane) * angle.abs / 360
  val isCurve: Boolean = true
  def radius: Int => Double = lane => pieceRadius + lanes.find(_.index == lane).map(x => angle / angle.abs * x.distanceFromCenter * -1).getOrElse(0.0)

  override def toString() = s"Curve: a: ${angle}, r: ${radius(666)}${if (switch) "(Switch)"}"
}
case class Straight(index: Int, pieceLength: Double, lanes: List[Lane], switch: Boolean = false) extends TrackPiece {
  val length: Int => Double = lane => pieceLength 
  val isCurve: Boolean = false

  override def toString() = s"Straight: ${length(0)}${if (switch) "(Switch)"}"
}



case class TrackHelper(pieces: List[Piece], lanes: List[Lane]) {
  var indexCounter = 0
  val track: List[TrackPiece] = pieces.map(piece => {
    val x = createPiece(piece, indexCounter)
    indexCounter = indexCounter + 1
    x
  })

  def createPiece(piece: Piece, index: Int): TrackPiece = {
    if (piece.angle.isDefined) 
      Curve(index, piece.angle.get, piece.radius.get, lanes, piece.switch.getOrElse(false))
    else 
      Straight(index, piece.length.get, lanes, piece.switch.getOrElse(false))
  }

  val MaxDrift = 57

  def trackFromIndex(index: Int) = track.drop(index) ++ track.take(index)
  def next(index: Int) = trackFromIndex(index).tail.head
  def nextDifferent(index: Int) = {
    val currentPiece = current(index)
    trackFromIndex(index).tail.dropWhile {
      case s: Straight => !currentPiece.isCurve
      case c: Curve => currentPiece.isCurve && currentPiece.asInstanceOf[Curve].radius(0) == c.radius(0)
    }.head
  }
  def current(index: Int) = trackFromIndex(index).head
  def angleOfNext(index: Int) = trackFromIndex(index).dropWhile(!_.isCurve).takeWhile(_.isCurve).map(x => x.asInstanceOf[Curve].angle).sum

  def distanceToNext(lane: Int, index: Int, inPieceDistance: Double) = track(index).length(lane) - inPieceDistance

  def distanceToNextDifferent(lane: Int, index: Int, inPieceDistance: Double) = track(index).length(lane) - inPieceDistance + 
    trackFromIndex(index).tail.takeWhile(x => x match {
      case s: Straight => !current(index).isCurve
      case c: Curve => current(index).isCurve && current(index).asInstanceOf[Curve].radius(lane) == c.radius(lane)
    }).map(_.length(lane)).sum

  def distanceToNextCurve(lane: Int, index: Int, inPieceDistance: Double) = track(index).length(lane) - inPieceDistance +
    trackFromIndex(index).tail.takeWhile(x => x match {
      case s: Straight => true
      case c: Curve => current(index).isCurve && current(index).asInstanceOf[Curve].radius(lane) == c.radius(lane)
    }).map(_.length(lane)).sum

  def nextCurve(index: Int) = {
    val currentPiece = current(index)
    trackFromIndex(index).tail.dropWhile {
      case s: Straight => true
      case c: Curve => currentPiece.isCurve && currentPiece.asInstanceOf[Curve].radius(0) == c.radius(0)
    }.head
  }


  var previousCrashings = List(false, false, false)
  def tooFast(speed: Double, angle: Double, slippage: Double, lane: Int, index: Int, inPieceDistance: Double, previousAngles: (Double, Double, Double), turbo: Boolean = false): Boolean = {
    def goingToCrash = notSafeToAccelerate(speed, angle, slippage, lane, index, inPieceDistance, previousAngles)
    def reachedBreakingDistance = distanceToNextCurve(lane, index, inPieceDistance) < calculateBreakingDistance(speed, lane, index, angle, turbo)
    def goingTooFast = speed > TrackHelper.maxSpeed(current(index), index, lane)
    if (speed <= 2.0) {
      false
    }
    else {
      val ret = reachedBreakingDistance || goingToCrash  || previousCrashings.contains(true) || slippage.abs > 5 || angle.abs > 50
      previousCrashings = previousCrashings.tail.::(goingToCrash)
      ret
    }
  }

  var deceleratePerTick = 0.13

  def ticksToDecelerate(speed: Double, nextSpeed: Double): Int = brakingFunctionSpeed(nextSpeed).round.toInt - brakingFunctionSpeed(speed).round.toInt + 1

  val a = 0.0025 / 2
  val b = - 0.16217
  val c = 8.26
  def brakingFunctionSpeed(y: Double): Double = ((b * -1) - Math.sqrt(b * b - 4 * a * (c - y))) / (2 * a)

  def calculateBreakingDistance(speed: Double, lane: Int, index: Int, angle: Double, turbo: Boolean = false) = {
    val maxSpeed = Math.max(4, TrackHelper.maxSpeed(nextCurve(index), nextCurve(index).index, lane))
    val avgSpeed = (speed + maxSpeed) / 2
    avgSpeed * (ticksToDecelerate(speed, maxSpeed) 
      - (5 - 5 * angle.abs / MaxDrift))
  }

  def notSafeToAccelerate(speed: Double, angle: Double, slippage: Double, lane: Int, index: Int, inPieceDistance: Double, previousAngles: (Double, Double, Double)): Boolean = {
    current(index).isCurve && 
      (angle.abs + expectedSlippage(speed, angle, slippage, lane, index, inPieceDistance, previousAngles)._1.abs > MaxDrift || expectedSlippage(speed, angle, slippage, lane, index, inPieceDistance, previousAngles)._2.abs - angle.abs > MaxDrift)
  }

  def expectedSlippage(speed: Double, angle: Double, slippage: Double, lane: Int, index: Int, inPieceDistance: Double, previousAngles: (Double, Double, Double)): (Double, Double) = {
    val ticksToNext = distanceToNextDifferent(lane, index, inPieceDistance ) / speed
    val expectedSlippage = (1 to Math.min(20, ticksToNext.toInt + 3)).scanLeft(0.0)((total: Double, count: Int) => {
      total + slippage + (count * rateOfChange(angle, previousAngles))
    })
    (expectedSlippage.max, expectedSlippage.min)
  }

  def rateOfChange(angle: Double, angles: (Double, Double, Double)): Double = {
    (angle.abs - angles._3.abs) - (angles._3.abs - angles._2.abs)
  }
}

object TrackHelper {

  var frictionMods: Map[Int, Double] = Map.empty

  def printValues(helper: TrackHelper) = {
    frictionMods.foreach(x => {
      val speed = maxSpeed(helper.track(x._1), x._1, RouteHelper.preferredLane(x._1, 0))
      println(s"Index: ${x._1} (${helper.track(x._1)}), mod: ${x._2}, speed: $speed")
    })
  }

  def maxSpeed(piece: TrackPiece, index: Int, lane: Int): Double = {
    piece match {
      case c: Curve => {        
        val mod = frictionMods.get(index).getOrElse(0.2)
        val speed = Math.sqrt(mod * c.radius(lane))
        speed
      }
      case _ => 10000.0
    }
  }

  def setMaxSpeed(piece: TrackPiece, index: Int, speed: Double, lane: Int) = {
    piece match {
      case c: Curve => {
        if (speed > maxSpeed(piece, index, lane) && speed < 11 && (speed - maxSpeed(piece, index, lane)).abs < 1.2  )
          frictionMods = frictionMods ++ Map(index -> speed * speed / c.radius(lane))
      }
      case _ => ()
    }
  }
}