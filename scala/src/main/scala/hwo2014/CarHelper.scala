package hwo2014

case class CarHelper(previous: CarPosition, current: CarPosition, helper: TrackHelper, turbo: Boolean = false) {	
  val angle = current.angle
  val slippage = current.angle.abs - previous.angle.abs
  val lane = current.piecePosition.lane.endLaneIndex
  val speed = calcSpeed(previous, current, helper)
  val index = current.piecePosition.pieceIndex
  val inPieceDistance = current.piecePosition.inPieceDistance
  val lap = current.piecePosition.lap

  def calcSpeed(previous: CarPosition, current: CarPosition, helper: TrackHelper): Double = {
    if (current.piecePosition.pieceIndex == previous.piecePosition.pieceIndex){
      current.piecePosition.inPieceDistance - previous.piecePosition.inPieceDistance
    } else {
      val currentPiece = helper.track(current.piecePosition.pieceIndex)
      val previousPiece = helper.track(previous.piecePosition.pieceIndex)
      val distanceTravelled = (previousPiece.length(previous.piecePosition.lane.startLaneIndex) - previous.piecePosition.inPieceDistance) + current.piecePosition.inPieceDistance
      distanceTravelled
    }
  }
}