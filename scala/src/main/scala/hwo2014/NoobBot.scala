package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: trackName :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, trackName, Some(true))
    case hostName :: port :: botName :: botKey ::  _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey, "suzuka")
    case _ => println("args missing")
  }
}



class NoobBot(host: String, port: Int, botName: String, botKey: String, trackName: String, specificRace: Option[Boolean] = Some(false)) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  if (specificRace.getOrElse(false)) {
    send(MsgWrapper("joinRace", JoinRace(BotId(botName, botKey), Some(trackName), carCount = 1, Some("123"))))
  }
  else {
    send(MsgWrapper("join", Join(botName, botKey)))
  }

  var previousPosition: CarPosition = null
  var trackHelper: TrackHelper = null
  var myCar: CarInfo = null
  var init: GameInit = null
  var turboOn = false
  var previousNames: Map[String, CarPosition] = Map.empty
  var names: Map[String, CarPosition] = Map.empty
  
  play

  @tailrec private def play {
    val line = reader.readLine()
    var response: MsgWrapper = MsgWrapper("throttle", 1.0)
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("yourCar", data) => {
          myCar = data.extract[CarInfo]
        }
        case MsgWrapper("carPositions", data) => {
          val positions = data.extract[List[CarPosition]]
          var names = positions.groupBy(_.id.name).mapValues(_(0))
          val current = names.get(myCar.name).getOrElse(positions(0))
          if (previousPosition == null) {
            RouteHelper.calculate(trackHelper, current.piecePosition.lane.endLaneIndex)
          } else {                      
            response = Logic.getNextMove(previousPosition, current, trackHelper, turboOn, names.filter(_._1 != myCar.name), previousNames)
            previousPosition = current
          }
          previousPosition = current
          previousNames = names
        }
        case MsgWrapper("gameInit", data) => {
          init = data.extract[GameInit]
          trackHelper = TrackHelper(init.race.track.pieces, init.race.track.lanes)
          previousPosition = null
          turboOn = false
          Logic.turbo = None
          Logic.throttle = 1.0
          Logic.previousSlippage = (0.0, 0.0, 0.0)
          Logic.previousAngles = (0.0, 0.0, 0.0)
          Logic.movingToLane = -1
        }
        case MsgWrapper("lapFinished", data) => {
          val finished = data.extract[LapFinished]
          println("Lap finished, Time: " + finished.lapTime.millis)
          println(TrackHelper.printValues(trackHelper))
          Logic.movingToLane = -1       
        }
        case MsgWrapper("turboAvailable", data) => {          
          val turbo = Some(data.extract[TurboAvailable])
          println("Turbo available!! " + turbo)
          Logic.turbo = turbo
        }
        case MsgWrapper("turboStart", data) => {          
          turboOn = true
        }
        case MsgWrapper("turboEnd", data) => {          
          turboOn = false
        }
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }
      send(response)
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Route(lane: Int, piece: TrackPiece, length: Double)

case class RouteCalculator(helper: TrackHelper, startLane: Int) {

  val routes = createRoute(helper.track.tail, routeOptions(helper.track.head, List.empty))
  val shortestRoute: List[Route] = {
    routes.minBy[Double](_.head.length).reverse
  }

  def createRoute(pieces: List[TrackPiece], route: List[List[Route]]): List[List[Route]] = {
    pieces match {
      case x :: xs => {
        createRoute(xs, route.flatMap(routeOptions(x, _)))
      }
      case Nil => route
    }
  }


  def routeOptions(piece: TrackPiece, currentRoute: List[Route]): List[List[Route]] = {
    val currentLane: Int = currentRoute.headOption.map(_.lane).getOrElse(startLane)
    val length: Double = currentRoute.headOption.map(_.length).getOrElse(0)
    if (piece.switch) {
      helper.lanes.filter(lane => {
        (currentLane - lane.index).abs <= 1 // Filter out lanes where car can't go.
      }).map(lane => Route(lane.index, piece, piece.length(currentLane) + length) :: currentRoute)   
    } else {
      List(Route(currentLane, piece, piece.length(currentLane) + length) :: currentRoute)
    }
  }
}

object RouteHelper {
  var shortestRoute: List[Route] = List.empty
  var routes: List[List[Route]] = List.empty

  def calculate(helper: TrackHelper, lane: Int) = {
    Future {
      routes = RouteCalculator(helper, lane).routes
      shortestRoute = routes.minBy[Double](_.head.length).reverse
    }
  }

  def preferredLane(index: Int, currentLane: Int): Int =
    if (shortestRoute.isEmpty) currentLane else (shortestRoute.drop(index) ++ shortestRoute.take(index)).tail.head.lane
}



object Logic {
  var throttle = 1.0
  var previousSlippage = (0.0, 0.0, 0.0)
  var previousAngles = (0.0, 0.0, 0.0)
  var movingToLane = -1
  var turbo: Option[TurboAvailable] = None

  def distance(a: CarHelper, b: CarHelper, helper: TrackHelper) = {
    val trackMaxIndex = helper.track.size - 1
    if (a.index == b.index) {
      if (a.inPieceDistance > b.inPieceDistance) {
        helper.track.foldLeft(0.0)((l, x) => l + x.length(a.lane))
      } else {
        a.inPieceDistance - b.inPieceDistance
      }
    } else {
      if (a.index > b.index) {
        (a.index to trackMaxIndex).map(helper.track(_).length(a.lane)).sum - a.inPieceDistance + (0 to (b.index - 1)).map(helper.track(_).length(b.lane)).sum + b.inPieceDistance
      } else {
        (a.index to b.index - 1).map(helper.track(_).length(a.lane)).sum - a.inPieceDistance + b.inPieceDistance
      }
    }
  }

  def getNextMove(previous: CarPosition, current: CarPosition, helper: TrackHelper, turboOn: Boolean, names: Map[String, CarPosition], previousNames: Map[String, CarPosition]): MsgWrapper = {
    val car = CarHelper(previous, current, helper, turboOn)
    val cars: Map[String, CarHelper] = names.map(x => (x._1, CarHelper(previousNames.get(x._1).getOrElse(x._2), x._2, helper, false)))
    // println(cars)
    val index = current.piecePosition.pieceIndex
    val inPieceDistance = current.piecePosition.inPieceDistance
    val nextThrottle = getThrottle(car, helper, index, inPieceDistance)
    val shortestRoute = RouteHelper.shortestRoute
     
    val carInFrontOfUs: Option[CarHelper] = cars.values.toList.sortBy(distance(car, _, helper)).headOption
    val preferredLane: Int = carInFrontOfUs.map(x => 
      if (x.speed < car.speed && distance(car, x, helper) < 100) {
        (RouteHelper.preferredLane(index, car.lane) - 1).abs
      } else {
        RouteHelper.preferredLane(index, car.lane)
      }).getOrElse(RouteHelper.preferredLane(index, car.lane))

    val turboDistance = turbo.map(x => x.turboDurationTicks * ((car.speed + 12 + x.turboFactor) / 2)).getOrElse(0.0)
    if (previous.piecePosition.lane.startLaneIndex != previous.piecePosition.lane.endLaneIndex && previous.piecePosition.lane.startLaneIndex != current.piecePosition.lane.startLaneIndex) {
      println("Recalculating route.")
      RouteHelper.calculate(helper, car.lane)
    }
    if ((preferredLane != car.lane && movingToLane != preferredLane)) {
      val switch = if (preferredLane > car.lane) "Right" else "Left"
      println(s"Switching lane: ${car.lane} -> $preferredLane")
      movingToLane = if (preferredLane > car.lane) car.lane + 1 else car.lane - 1
      MsgWrapper("switchLane", switch)
    }
   else if (turbo.isDefined && !helper.current(index).isCurve && helper.distanceToNextDifferent(car.lane, index, inPieceDistance) > turboDistance) {
      turbo = None
      println(s"Activating TURBO! ${helper.distanceToNextDifferent(car.lane, index, inPieceDistance)} > $turboDistance")
      MsgWrapper("turbo", "Go fastttttttt")
   } else {
      throttle = nextThrottle
      MsgWrapper("throttle", nextThrottle)
    }
  }



  def slippageAvg = (previousSlippage._1 + previousSlippage._2 + previousSlippage._3) / 3

  var previousThrottle = 0.0
  def getThrottle(car: CarHelper, helper: TrackHelper, index: Int, inPieceDistance: Double): Double = {
    val result = if (helper.tooFast(car.speed, car.angle, car.slippage, car.lane, index, inPieceDistance, previousAngles, car.turbo)) 0.0 else 1.0
    val expectedSlippage = helper.expectedSlippage(car.speed, car.angle, car.slippage, car.lane, index, inPieceDistance, previousAngles)
    if (result == 1.0 && previousThrottle == 1.0) {
      TrackHelper.setMaxSpeed(helper.current(index), index, car.speed, car.lane)
    }
    previousThrottle = result
    println(s"V: ${car.speed}/${TrackHelper.maxSpeed(helper.track(index), index, car.lane)}, A ${car.angle}, T: $result, dS: ${expectedSlippage} - ${index}: ${helper.current(index)} -> ${helper.distanceToNextDifferent(car.lane, index, inPieceDistance)} ${helper.next(index)}")
    previousAngles = (previousAngles._2, previousAngles._3, car.angle)
    previousSlippage = (previousSlippage._2, previousSlippage._3, car.slippage)
    result
  }
}


// Car
case class Car(id: CarInfo, dimensions: CarDimensions)
case class CarInfo(name: String, color: String)
case class CarDimensions(length: Double, width: Double, guideFlagPosition: Double)

// Game Init
case class GameInit(race: Race)
case class Race(track: Track, cars: List[Car], raceSession: RaceSession)
case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint)
case class Lane(index: Int, distanceFromCenter: Int)
case class Piece(length: Option[Double], switch: Option[Boolean], angle: Option[Double], radius: Option[Double])
case class StartingPoint(position: Position, angle: Double)
case class Position(x: Double, y: Double)
case class RaceSession(laps: Option[Int], maxLapTimeMs: Option[Int], quickRace: Option[Boolean], durationMs: Option[Double])

// Car Position
case class CarPosition(id: CarInfo, angle: Double, piecePosition: PiecePosition)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: SwitchLane, lap: Int)
case class SwitchLane(startLaneIndex: Int, endLaneIndex: Int)

// Info
case class LapFinished(car: CarInfo, lapTime: ResultInfo, raceTime: ResultInfo, ranking: Ranking)
case class TurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Double, turboFactor: Double)
case class ResultInfo(laps: Option[Int], ticks: Option[Int], millis: Option[Int])
case class Ranking(overall: Int, fastestLap: Int)


// Messages
case class Join(name: String, key: String)
case class JoinRace(botId: BotId, trackName: Option[String], carCount: Int, password: Option[String])
case class BotId(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue)

object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
